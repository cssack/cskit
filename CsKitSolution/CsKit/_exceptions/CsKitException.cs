﻿using System;
using System.Runtime.Serialization;

namespace CsKit._exceptions
{
	public class CsKitException : Exception
	{
		public CsKitException() { }

		public CsKitException(string message) : base(message) { }

		public CsKitException(string message, Exception innerException) : base(message, innerException) { }

		protected CsKitException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}