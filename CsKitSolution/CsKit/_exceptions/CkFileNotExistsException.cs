﻿using System;
using System.Runtime.Serialization;



namespace CsKit._exceptions
{
	public class CkFileNotExistsException : CsKitException
	{
		public CkFileNotExistsException()
		{
		}


		public CkFileNotExistsException(string message) : base(message)
		{
		}


		public CkFileNotExistsException(string message, Exception innerException) : base(message, innerException)
		{
		}


		protected CkFileNotExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}