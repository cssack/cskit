﻿using System;
using System.Runtime.Serialization;



namespace CsKit._exceptions
{
	public class CkFileAlreadyExistsException : CsKitException
	{
		public CkFileAlreadyExistsException()
		{
		}


		public CkFileAlreadyExistsException(string message) : base(message)
		{
		}


		public CkFileAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
		{
		}


		protected CkFileAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}