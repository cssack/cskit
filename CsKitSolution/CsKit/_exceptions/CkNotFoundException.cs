﻿using System;
using System.Runtime.Serialization;

namespace CsKit._exceptions
{
	public class CkNotFoundException : CsKitException
	{
		public CkNotFoundException()
		{
		}

		public CkNotFoundException(string message) : base(message)
		{
		}

		public CkNotFoundException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected CkNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}