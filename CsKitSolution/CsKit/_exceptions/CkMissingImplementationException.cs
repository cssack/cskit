﻿using System;
using System.Runtime.Serialization;

namespace CsKit._exceptions
{
	public class CkMissingImplementationException : CsKitException
	{
		public CkMissingImplementationException() { }

		public CkMissingImplementationException(string message) : base(message) { }

		public CkMissingImplementationException(string message, Exception innerException) : base(message, innerException) { }

		protected CkMissingImplementationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}