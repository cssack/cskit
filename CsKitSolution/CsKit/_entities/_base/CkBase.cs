﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using CsKit._extensions;
using CsKit._extensions.typed;



namespace CsKit._entities._base
{
	/// <summary>
	///     A useful base class for all posible classes used within WPF or need access to properties or fields via reflection.
	/// </summary>
	public class CkBase : INotifyPropertyChanged, INotifyPropertyChanging
	{
		[field: NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		[field: NonSerialized]
		public event PropertyChangingEventHandler PropertyChanging;

		/// <summary>
		///     Invokes the <see cref="PropertyChanged" /> event. Use this function to manually update WPF UI or other stuff.
		///     Prefere using the <see cref="Set{T}(ref T,T,string)" /> method instead of manually fire the event.
		/// </summary>
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

		/// <summary>
		///     Invokes the <see cref="PropertyChanging" /> event.
		/// </summary>
		protected virtual void OnPropertyChanging([CallerMemberName] string propertyName = null) => PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));

		/// <summary>
		///     Use this in each property of the class as the setter method. This will enable automatic
		///     <see cref="PropertyChanged" /> and <see cref="PropertyChanging" /> invokations. The function ensures that the
		///     events will only be fired if the value changes. Returns true if the value has been changed.
		/// </summary>
		protected virtual bool Set<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
		{
			if (Equals(field, value))
				return false;
			OnPropertyChanging(propertyName);
			field = value;
			OnPropertyChanged(propertyName);

			return true;
		}

		/// <summary>
		///     Use this in each property of the class as the getter method. This will allow future get function adaptations
		/// </summary>
		protected virtual T Get<T>(ref T field, [CallerMemberName] string propertyName = null) => field;


		public override string ToString()
		{
			var type = GetType();
			var properties = type.ReflectMembers().Values.OfType<PropertyInfo>().Where(x => x.PropertyType.IsNullable() || x.PropertyType.IsPrimitive || x.PropertyType == typeof(string));
			var valueCollection = properties.Select(x => $"{x.Name}: '{this.Get(x)}'");
			return $"{type.Name}{{{valueCollection.Join()}}}";
		}
	}
}