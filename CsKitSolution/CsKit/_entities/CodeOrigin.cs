﻿using System.Runtime.CompilerServices;
using CsKit._entities._base;



namespace CsKit._entities
{
	public class CodeOrigin : CkBase
	{
		private string _file;
		private int _lineNumber;
		private string _method;


		public CodeOrigin([CallerFilePath]string file = default(string), [CallerLineNumber]int lineNumber = default(int), [CallerMemberName]string method = default(string))
		{
			_file = file;
			_lineNumber = lineNumber;
			_method = method;
		}


		public string File
		{
			get => Get(ref _file);
			set => Set(ref _file, value);
		}
		public string Method
		{
			get => Get(ref _method);
			set => Set(ref _method, value);
		}
		public int LineNumber
		{
			get => Get(ref _lineNumber);
			set => Set(ref _lineNumber, value);
		}
	}
}