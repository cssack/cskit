﻿using System;
using System.Data.Common;
using CsKit._exceptions;



namespace CsKit._entities.auth
{
	public class DbAuth : LoginAuth
	{
		private readonly Func<DbAuth, DbConnection> _newConnection;

		private string _dataBase;
		private string _server;
		private ServerTypes _serverType;


		public DbAuth(Func<DbAuth, DbConnection> newConnection) =>
			_newConnection = newConnection;


		public ServerTypes ServerType
		{
			get => Get(ref _serverType);
			set => Set(ref _serverType, value);
		}
		public string Server
		{
			get => Get(ref _server);
			set => Set(ref _server, value);
		}
		public string DataBase
		{
			get => Get(ref _dataBase);
			set => Set(ref _dataBase, value);
		}


		public DbConnection NewConnection() =>
			_newConnection(this);


		public string ToConnectionString()
		{
			switch (ServerType)
			{
				case ServerTypes.SqlServer: return $"Server={Server};Database={DataBase};User Id={User};Password={Password};";
				default: throw new CkMissingImplementationException($"The {nameof(ServerType)}.{ServerType} is not supported.");
			}
		}




		public enum ServerTypes
		{
			SqlServer
		}
	}
}