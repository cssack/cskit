﻿using CsKit._entities._base;



namespace CsKit._entities.auth
{
	public class LoginAuth : CkBase
	{
		private string _password;
		private string _user;

		public string User { get => Get(ref _user); set => Set(ref _user, value); }
		public string Password { get => Get(ref _password); set => Set(ref _password, value); }
	}
}