﻿using System;



namespace CsKit._extensions.typed
{
	public static class DateTimeExtensions
	{
		private static readonly DateTime Epoch = new DateTime(
			1970,
			1,
			1,
			0,
			0,
			0,
			DateTimeKind.Utc
		);


		public static DateTime FromUnixTime(this long unixTime) =>
			Epoch.AddSeconds(unixTime);
	}
}