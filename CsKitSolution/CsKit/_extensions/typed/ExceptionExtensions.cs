﻿using System;
using System.Collections.Generic;
using System.Text;
using CsKit._extensions._exceptions;



namespace CsKit._extensions.typed
{
	/// <summary>
	///     Extensions build by Christian Sack. Used to improve code readability.
	/// </summary>
	public static class ExceptionExtensions
	{
		/// <summary>Returns the most inner exception in a exception. Traversing the inner exception.</summary>
		public static Exception MostInner(this Exception exception)
		{
			exception.MayNotBeNull(nameof(exception));

			var current = exception;
			while (current?.InnerException != null)
				current = current.InnerException;
			return current;
		}
	}
}
