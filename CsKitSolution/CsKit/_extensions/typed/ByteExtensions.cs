﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using CsKit._extensions._exceptions;



namespace CsKit._extensions.typed
{
	/// <summary>
	///     Extensions build by Christian Sack. Used to improve code readability.
	/// </summary>
	public static class ByteExtensions
	{
		/// <summary>
		///     SHA-1 produces a 160-bit (20-byte) hash value known as a message digest. A SHA-1 hash value is typically rendered
		///     as a hexadecimal number, 40 digits long.
		/// </summary>
		public static byte[] ToSha1Hash(this byte[] input)
		{
			input.MayNotBeNull(nameof(input));
			using (var sha1 = new SHA1CryptoServiceProvider())
				return sha1.ComputeHash(input);
		}


		/// <summary>
		///     SHA-256 produces a 256-bit (32-byte) hash value known as a message digest. A SHA-256 hash value is typically
		///     rendered as a hexadecimal number, 64 digits long.
		/// </summary>
		public static byte[] ToSha256Hash(this byte[] input)
		{
			input.MayNotBeNull(nameof(input));
			using (var sha = new SHA256CryptoServiceProvider())
				return sha.ComputeHash(input);
		}


		/// <summary>
		///     MD5 produces a 128-bit (16-byte) hash value known as a message-digest. The Algorithm is a widely used cryptographic
		///     hash function producing a hash value, typically expressed in text format as a 32 digit hexadecimal number. MD5 has
		///     been
		///     utilized in a wide variety of cryptographic applications, and is also commonly used to verify data integrity.
		/// </summary>
		public static byte[] ToMd5Hash(this byte[] input)
		{
			input.MayNotBeNull(nameof(input));
			using (var md5 = new MD5CryptoServiceProvider())
				return md5.ComputeHash(input);
		}


		/// <summary>
		///     Converts an <see cref="SerializableAttribute" /> object into an byte[] by using the
		///     <see cref="BinaryFormatter" />.
		/// </summary>
		public static object ToDeserializedObject(this byte[] input)
		{
			input.MayNotBeNull(nameof(input));
			using (var ms = new MemoryStream(input))
				return new BinaryFormatter().Deserialize(ms);
		}


		/// <summary>
		///     Converts an <see cref="SerializableAttribute" /> object into an byte[] by using the
		///     <see cref="BinaryFormatter" />.
		/// </summary>
		public static T ToDeserializedObject<T>(this byte[] input)
		{
			input.MayNotBeNull(nameof(input));
			return (T) input.ToDeserializedObject();
		}


		/// <summary>
		///     Fills the <paramref name="input" /> with a secure random sequence of bytes. Using the
		///     <see cref="RNGCryptoServiceProvider" />.
		/// </summary>
		public static byte[] ToSecureRandomBytes(this byte[] input)
		{
			input.MayNotBeNull(nameof(input));
			using (var rng = new RNGCryptoServiceProvider())
				rng.GetBytes(input);
			return input;
		}


		/// <summary>
		///     Converts the specified byte array into a base64 encoded string.
		/// </summary>
		public static string ToBase64(this byte[] val, int offset = 0, int? length = null, Base64FormattingOptions formatting = Base64FormattingOptions.None)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length);
			length?.MayNotBeGreaterThen(nameof(length), val.Length - offset);
			return Convert.ToBase64String(val, offset, length.GetValueOrDefault(val.Length), formatting);
		}


		/// <summary>
		///     Copies the array (or a part of the array) into a new byte array
		/// </summary>
		public static byte[] Copy(this byte[] val, int offset = 0, int? length = null)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length);
			length?.MayNotBeGreaterThen(nameof(length), val.Length - offset);

			var bytes = new byte[length ?? val.Length];
			Buffer.BlockCopy(val, offset, bytes, 0, length ?? val.Length);
			return bytes;
		}


		/// <summary>
		///     Reads a <see cref="short" /> from the byte[] at a given <paramref name="offset" />.
		/// </summary>
		public static short ReadInt16(this byte[] val, int offset, bool littleEndian = true)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length-sizeof(short));

			if (BitConverter.IsLittleEndian == littleEndian)
				return BitConverter.ToInt16(val, offset);

			return (short) ((val[offset + 0] << 8)
			                | val[offset + 1]);
		}


		/// <summary>
		///     Reads a <see cref="int" /> from the byte[] at a given <paramref name="offset" />.
		/// </summary>
		public static int ReadInt32(this byte[] val, int offset, bool littleEndian = true)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length-sizeof(int));

			if (BitConverter.IsLittleEndian == littleEndian)
				return BitConverter.ToInt32(val, offset);

			return (val[offset + 0] << 24)
			       | (val[offset + 1] << 16)
			       | (val[offset + 2] << 8)
			       | val[offset + 3];
		}


		/// <summary>
		///     Reads a <see cref="long" /> from the byte[] at a given <paramref name="offset" />.
		/// </summary>
		public static long ReadInt64(this byte[] val, int offset, bool littleEndian = true)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length-sizeof(long));

			if (BitConverter.IsLittleEndian == littleEndian)
				return BitConverter.ToInt64(val, offset);

			return ((long) val[offset + 0] << 56)
			       | ((long) val[offset + 1] << 48)
			       | ((long) val[offset + 2] << 40)
			       | ((long) val[offset + 3] << 32)
			       | ((long) val[offset + 4] << 24)
			       | ((long) val[offset + 5] << 16)
			       | ((long) val[offset + 6] << 8)
			       | val[offset + 7];
		}


		/// <summary>
		///     Reads a <see cref="ushort" /> from the byte[] at a given <paramref name="offset" />.
		/// </summary>
		public static ushort ReadUInt16(this byte[] val, int offset, bool littleEndian = true)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length-sizeof(ushort));

			if (BitConverter.IsLittleEndian == littleEndian)
				return BitConverter.ToUInt16(val, offset);

			return (ushort) ((val[offset + 0] << 8)
			                 | (val[offset + 1] << 0));
		}


		/// <summary>
		///     Reads a <see cref="uint" /> from the byte[] at a given <paramref name="offset" />.
		/// </summary>
		public static uint ReadUInt32(this byte[] val, int offset, bool littleEndian = true)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length-sizeof(uint));

			if (BitConverter.IsLittleEndian == littleEndian)
				return BitConverter.ToUInt32(val, offset);

			return ((uint) val[offset + 0] << 24)
			       | ((uint) val[offset + 1] << 16)
			       | ((uint) val[offset + 2] << 8)
			       | ((uint) val[offset + 3] << 0);
		}


		/// <summary>
		///     Reads a <see cref="ulong" /> from the byte[] at a given <paramref name="offset" />.
		/// </summary>
		public static ulong ReadUInt64(this byte[] val, int offset, bool littleEndian = true)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length-sizeof(ulong));

			if (BitConverter.IsLittleEndian == littleEndian)
				return BitConverter.ToUInt64(val, offset);

			return ((ulong) val[offset + 0] << 56)
			       | ((ulong) val[offset + 1] << 48)
			       | ((ulong) val[offset + 2] << 40)
			       | ((ulong) val[offset + 3] << 32)
			       | ((ulong) val[offset + 4] << 24)
			       | ((ulong) val[offset + 5] << 16)
			       | ((ulong) val[offset + 6] << 8)
			       | ((ulong) val[offset + 7] << 0);
		}

		/// <summary>
		///     Reads a <see cref="ulong" /> from the byte[] at a given <paramref name="offset" />.
		/// </summary>
		public static byte[] ReadBytes(this byte[] val, int offset, int? length = null)
		{
			val.MayNotBeNull(nameof(val));
			offset.MayNotBeNegative(nameof(offset));
			offset.MayNotBeGreaterThen(nameof(offset), val.Length-sizeof(ulong));
			length?.MayNotBeZeroOrNegative(nameof(length));
			length?.MayNotBeGreaterThen(nameof(length), val.Length-offset);

			return val.Copy(offset, length);
		}

		
		/// <summary>
		///     Writes the content of <paramref name="source"/> to the <paramref name="target"/> at the specified <paramref name="targetOffset"/>.
		/// </summary>
		public static void Write(this byte[] target, int targetOffset, byte[] source, int sourceOffset = 0, int? length = null)
		{
			target.MayNotBeNull(nameof(target));
			target.Length.MayNotBeSmallerThen(nameof(target), length??source.Length);
			source.MayNotBeNull(nameof(source));
			targetOffset.MayNotBeNegative(nameof(targetOffset));
			targetOffset.MayNotBeGreaterThen(nameof(targetOffset), target.Length);
			sourceOffset.MayNotBeNegative(nameof(sourceOffset));
			sourceOffset.MayNotBeGreaterThen(nameof(sourceOffset), source.Length);
			length?.MayNotBeZeroOrNegative(nameof(length));
			length?.MayNotBeGreaterThen(nameof(length), source.Length-targetOffset);


			Buffer.BlockCopy(source, sourceOffset, target, targetOffset, length ?? source.Length);
		}

		
		/// <summary>
		///     Writes the <paramref name="value"/> to the <paramref name="target"/> at the specified <paramref name="targetOffset"/>.
		/// </summary>
		public static void Write(this byte[] target, int targetOffset, int value, bool littleEndian = true)
		{
			target.MayNotBeNull(nameof(target));
			target.Length.MayNotBeSmallerThen(nameof(target), sizeof(int));
			targetOffset.MayNotBeNegative(nameof(targetOffset));
			targetOffset.MayNotBeGreaterThen(nameof(targetOffset), target.Length - sizeof(int));

			if (BitConverter.IsLittleEndian == littleEndian)
			{
				target[targetOffset + 0] = (byte) (value >> 0);
				target[targetOffset + 1] = (byte) (value >> 8);
				target[targetOffset + 2] = (byte) (value >> 16);
				target[targetOffset + 3] = (byte) (value >> 24);
			}
			else
			{
				target[targetOffset + 0] = (byte) (value >> 24);
				target[targetOffset + 1] = (byte) (value >> 16);
				target[targetOffset + 2] = (byte) (value >> 8);
				target[targetOffset + 3] = (byte) (value >> 0);
			}
		}


		public static void Write(this byte[] target, int targetOffset, uint value, bool littleEndian = true)
		{
			target.MayNotBeNull(nameof(target));
			target.Length.MayNotBeSmallerThen(nameof(target), sizeof(uint));
			targetOffset.MayNotBeNegative(nameof(targetOffset));
			targetOffset.MayNotBeGreaterThen(nameof(targetOffset), target.Length - sizeof(uint));

			if (BitConverter.IsLittleEndian == littleEndian)
			{
				target[targetOffset + 0] = (byte) (value >> 0);
				target[targetOffset + 1] = (byte) (value >> 8);
				target[targetOffset + 2] = (byte) (value >> 16);
				target[targetOffset + 3] = (byte) (value >> 24);
			}
			else
			{
				target[targetOffset + 0] = (byte) (value >> 24);
				target[targetOffset + 1] = (byte) (value >> 16);
				target[targetOffset + 2] = (byte) (value >> 8);
				target[targetOffset + 3] = (byte) (value >> 0);
			}
		}
		
	}
}