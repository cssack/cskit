﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using CsKit._extensions._exceptions;



namespace CsKit._extensions.typed
{
	/// <summary>
	///     Extensions build by Christian Sack. Used to improve code readability.
	/// </summary>
	public static class ObjectExtensions
	{
		/// <summary>
		///     True if the provided <paramref name="val" /> is null.
		/// </summary>
		public static bool IsNull(this object val) =>
			val == null;


		/// <summary>
		///     Converts an <see cref="SerializableAttribute" /> object into an byte[] by using the
		///     <see cref="BinaryFormatter" />.
		/// </summary>
		public static byte[] ToSerializedBytes(this object input)
		{
			input.MayNotBeNull(nameof(input));
			using (var ms = new MemoryStream())
			{
				new BinaryFormatter().Serialize(ms, input);
				return ms.ToArray();
			}
		}
	}
}