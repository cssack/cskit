﻿using System;
using System.Collections.Generic;
using CsKit._extensions._exceptions;



namespace CsKit._extensions.typed
{
	/// <summary>
	///     Extensions build by Christian Sack. Used to improve code readability.
	/// </summary>
	public static class EnumerableExtensions
	{
		/// <summary>
		///     True if the length of <paramref name="val" /> is 0.
		/// </summary>
		public static bool IsEmpty<T>(this T[] val)
		{
			val.MayNotBeNull(nameof(val));
			return val.Length == 0;
		}


		/// <summary>
		///     True if the provided <paramref name="val" /> is null or its length is 0.
		/// </summary>
		public static bool IsNullOrEmpty<T>(this T[] val) =>
			val.IsNull() || val.IsEmpty();




		/// <summary>Finds the object where a specific value is the maximum in that list and returns the object itself.</summary>
		public static T MaxObject<T, TU>(this IEnumerable<T> source, Func<T, TU> selector) where TU : IComparable<TU>
		{
			source.MayNotBeNull(nameof(source));
			selector.MayNotBeNull(nameof(selector));

			var first  = true;
			var maxObj = default(T);
			var maxKey = default(TU);
			foreach (var item in source)
				if (first)
				{
					maxObj = item;
					maxKey = selector(maxObj);
					first = false;
				}
				else
				{
					var currentKey = selector(item);
					if (currentKey.CompareTo(maxKey) <= 0)
						continue;
					maxKey = currentKey;
					maxObj = item;
				}

			return maxObj;
		}


		/// <summary>Finds the object where a specific value is the maximum in that list and returns the object itself.</summary>
		public static T MinObject<T, TU>(this IEnumerable<T> source, Func<T, TU> selector) where TU : IComparable<TU>
		{
			source.MayNotBeNull(nameof(source));
			selector.MayNotBeNull(nameof(selector));

			var first  = true;
			var minObj = default(T);
			var minKey = default(TU);
			foreach (var item in source)
				if (first)
				{
					minObj = item;
					minKey = selector(minObj);
					first = false;
				}
				else
				{
					var currentKey = selector(item);
					if (currentKey.CompareTo(minKey) >= 0)
						continue;
					minKey = currentKey;
					minObj = item;
				}

			return minObj;
		}
	}
}