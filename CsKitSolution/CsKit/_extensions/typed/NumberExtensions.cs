﻿using System.Security.Cryptography;
using CsKit._extensions._exceptions;



namespace CsKit._extensions.typed
{
	/// <summary>
	///     Extensions build by Christian Sack. Used to improve code readability.
	/// </summary>
	public static class NumberExtensions
	{
		/// <summary>
		///     Generates a secure random sequence of bytes of <paramref name="length" />. Using the
		///     <see cref="RNGCryptoServiceProvider" />.
		/// </summary>
		public static byte[] ToSecureRandomBytes(this int length)
		{
			length.MayNotBeZeroOrNegative(nameof(length));
			return new byte[length].ToSecureRandomBytes();
		}


		/// <summary>
		///     true if <paramref name="val" /> is zero.
		/// </summary>
		public static bool IsZero(this int val) =>
			val == 0;


		/// <summary>
		///     true if <paramref name="val" /> is negative.
		/// </summary>
		public static bool IsNegative(this int val) =>
			val < 0;

		
		/// <summary>
		///     true if <paramref name="val" /> is zero or negative.
		/// </summary>
		public static bool IsZeroOrNegative(this int val) =>
			val <= 0;
	}
}