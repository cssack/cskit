﻿using System;
using System.Collections.Generic;
using CsKit._extensions._exceptions;



namespace CsKit._extensions.typed
{
	/// <summary>
	///     Extensions build by Christian Sack. Used to improve code readability.
	/// </summary>
	public static class TypeExtensions
	{
		private static readonly HashSet<Type> NetNumerics = new HashSet<Type>(new[]
		{
			typeof(byte),
			typeof(sbyte),
			typeof(ushort),
			typeof(uint),
			typeof(ulong),
			typeof(short),
			typeof(int),
			typeof(long),
			typeof(double),
			typeof(decimal),
			typeof(float)
		});

		/// <summary>
		///     Checks if type or its not nullable version is one of the following types:
		///     <para>
		///         <c>byte, ushort, uint, ulong, short, int, long, double, decimal, float,</c>
		///     </para>
		/// </summary>
		public static bool IsNumericType(this Type type)
		{
			type.MayNotBeNull(nameof(type));
			return NetNumerics.Contains(type.ToNonNullable());
		}


		/// <summary>Checks if the type is a null able.</summary>
		public static bool IsNullable(this Type type)
		{
			type.MayNotBeNull(nameof(type));
			return Nullable.GetUnderlyingType(type) != null;
		}


		/// <summary>Checks if the type is a null able and if it is it gives back the underlaying type.</summary>
		public static bool IsNullable(this Type type, out Type underlayingType)
		{
			type.MayNotBeNull(nameof(type));
			var innerType = Nullable.GetUnderlyingType(type);
			underlayingType = innerType ?? type;
			return innerType != null;
		}


		/// <summary>
		///     If <paramref name="type" /> type can be declared as <paramref name="target" />.
		/// </summary>
		public static bool IsDeclareableBy(this Type type, Type target)
		{
			type.MayNotBeNull(nameof(type));
			target.MayNotBeNull(nameof(target));
			return type.IsSubclassOf(target) || type == target;
		}


		/// <summary>
		///     If <paramref name="type" /> is <see cref="Nullable" /> return the underlaying type otherwise return
		///     <paramref name="type" />.
		/// </summary>
		public static Type ToNonNullable(this Type type)
		{
			type.MayNotBeNull(nameof(type));
			return type.IsNullable(out var real) ? real : type;
		}
	}
}
