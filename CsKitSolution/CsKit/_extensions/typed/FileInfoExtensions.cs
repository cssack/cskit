﻿using System.IO;



namespace CsKit._extensions.typed
{
	public static class FileInfoExtensions
	{
		/// <summary>
		///     Deletes a file if it exists.
		/// </summary>
		public static FileInfo Delete_IfExists(this FileInfo fi)
		{
			fi.Refresh();
			if (fi.Exists)
				fi.Delete();
			return fi;
		}
	}
}