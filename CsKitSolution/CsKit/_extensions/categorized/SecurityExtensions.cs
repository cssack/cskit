﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using CsKit._extensions.typed;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;



namespace CsKit._extensions.categorized
{
	/// <summary>
	///     Extensions build by Christian Sack. Used to improve code readability.
	/// </summary>
	public static class SecurityExtensions
	{
		/// <summary>
		///     Generates a password Hash (including salt) using the same method and format as
		///     https://github.com/aspnet/Identity/blob/a8ba99bc5b11c5c48fc31b9b0532c0d6791efdc8/src/Microsoft.AspNetCore.Identity/PasswordHasher.cs.
		/// </summary>
		public static byte[] ToPasswordHash(this string plainTextPassword) =>
			HashPasswordV3(plainTextPassword,
			               KeyDerivationPrf.HMACSHA256,
			               10000,
			               128 / 8,
			               256 / 8);


		public static bool VerifyPasswordHash(this string plainTextPassword, byte[] hashedPassword, out int iterCount)
		{
			try
			{
				// Read header information
				var prf = (KeyDerivationPrf) hashedPassword.ReadUInt32(1, false);
				iterCount = hashedPassword.ReadInt32(5, false);
				var saltLength   = hashedPassword.ReadInt32(9, false);
				var salt         = hashedPassword.ReadBytes(13, saltLength);
				var passwordHash = hashedPassword.ReadBytes(13 + salt.Length, hashedPassword.Length - 13 - salt.Length);

				// Hash the incoming password and verify it
				var actualSubkey = KeyDerivation.Pbkdf2(plainTextPassword, salt, prf, iterCount, passwordHash.Length);
				return passwordHash.SecureCompare(actualSubkey);
			}
			catch
			{
				// This should never occur except in the case of a malformed payload, where
				// we might go off the end of the array. Regardless, a malformed payload
				// implies verification failed.
				iterCount = default(int);
				return false;
			}
		}


		/// <summary>
		///     A secure way to compare two byte arrays avoiding time-based side channel attacks. It ist specifically written such
		///     that the loop is not optimized. Use this method for password comparision.
		/// </summary>
		[MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
		public static bool SecureCompare(this IReadOnlyList<byte> a, IReadOnlyList<byte> b)
		{
			if (a == null && b == null)
				return true;
			if (a == null || b == null || a.Count != b.Count)
				return false;

			var areSame = true;
			for (var i = 0; i < a.Count; i++)
				areSame &= a[i] == b[i];
			return areSame;
		}


		private static byte[] HashPasswordV3(string plainTextPassword, KeyDerivationPrf prf, int iterCount, int saltSize, int numBytesRequested)
		{
			var salt         = saltSize.ToSecureRandomBytes();
			var passwordHash = KeyDerivation.Pbkdf2(plainTextPassword, salt, prf, iterCount, numBytesRequested);

			var outputBytes = new byte[13 + salt.Length + passwordHash.Length];
			outputBytes[0] = 0x01; // format marker
			outputBytes.Write(1, (uint) prf, false);
			outputBytes.Write(5, iterCount, false);
			outputBytes.Write(9, saltSize, false);
			outputBytes.Write(13, salt);
			outputBytes.Write(13 + saltSize, passwordHash);
			return outputBytes;
		}
	}
}