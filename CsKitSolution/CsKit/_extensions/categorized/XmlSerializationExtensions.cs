﻿using System.IO;
using System.Xml.Serialization;
using CsKit._exceptions;



namespace CsKit._extensions.categorized
{
	public static class XmlSerializationExtensions
	{
		/// <summary>
		///     Serializes the <paramref name="obj" /> to xml and stores the result in a file.
		/// </summary>
		public static void ToXmlFile<T>(this T obj, FileInfo fi)
		{
			fi.Refresh();

			if (fi.Exists)
				throw new CkFileAlreadyExistsException();

			using (var fs = fi.Open(FileMode.Create))
			{
				var xs = new XmlSerializer(typeof(T));
				xs.Serialize(fs, obj);
			}
		}


		/// <summary>
		///     Deserializes the <paramref name="fi" /> from xml.
		/// </summary>
		public static T FromXmlFile<T>(this FileInfo fi)
		{
			fi.Refresh();

			if (!fi.Exists)
				throw new CkFileNotExistsException();

			using (var fs = fi.Open(FileMode.Open))
			{
				var xs = new XmlSerializer(typeof(T));
				return (T) xs.Deserialize(fs);
			}
		}
	}
}