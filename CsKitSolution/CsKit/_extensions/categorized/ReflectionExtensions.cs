﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using CsKit._exceptions;
using CsKit._extensions.typed;
using CsKit._extensions._exceptions;



namespace CsKit._extensions
{
	public static class ReflectionExtensions
	{
		private const BindingFlags Flags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;


		/// <summary>
		///     Returns all attributes assigned to the member.
		/// </summary>
		public static ImmutableArray<Attribute> ReflectAttributes(this MemberInfo memberInfo)
		{
			memberInfo.MayNotBeNull(nameof(memberInfo));
			return Cache.MemberAttributes.GetOrAdd(memberInfo, info => Reflection.Attributes(memberInfo).ToImmutableArray());
		}


		/// <summary>
		///     Returns all members including fields and properties.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static ImmutableDictionary<string, MemberInfo> ReflectMembers(this Type type)
		{
			type.MayNotBeNull(nameof(type));
			return Cache.TypeMembers.GetOrAdd(type, t => Reflection.Members(t).ToImmutableDictionary(x => x.Name, x => x));
		}


		/// <summary>
		///     Returns all <see cref="MemberInfo" />s which are attributed with by attributes with type
		///     <paramref name="attributeType" />.
		/// </summary>
		public static ImmutableDictionary<MemberInfo, ImmutableArray<Attribute>> ReflectMembersWithAttribute(this Type type, Type attributeType)
		{
			type.MayNotBeNull(nameof(type));
			attributeType.MayNotBeNull(nameof(attributeType));
			if (Cache.MembersWithAttribute.GetOrAdd(type,
			                                        t =>
			                                        {
				                                        return t.ReflectMembers()
				                                                .Values
				                                                .SelectMany(x => x.ReflectAttributes().Select(y => (Member: x, Attribute: y)))
				                                                .GroupBy(x => x.Attribute.GetType())
				                                                .ToImmutableDictionary(x => x.Key, x => x.GroupBy(a => a.Member).ToImmutableDictionary(a => a.Key, a => a.Select(b => b.Attribute).ToImmutableArray()));
			                                        })
			         .TryGetValue(attributeType, out var result))
				return result;
			return ImmutableDictionary<MemberInfo, ImmutableArray<Attribute>>.Empty;
		}


		/// <summary>
		///     Returns the type of the <paramref name="memberInfo" />.
		/// </summary>
		public static Type GetFieldOrPropertyType(this MemberInfo memberInfo)
		{
			memberInfo.MayNotBeNull(nameof(memberInfo));
			switch (memberInfo)
			{
				case PropertyInfo pi: return pi.PropertyType;
				case FieldInfo fi: return fi.FieldType;
				case Type t: return t;
				default: throw new CsException.NotImplemented($"Cannot extract type information for [{typeof(MemberInfo)}] of type [{memberInfo.GetType()}]");
			}
		}


		/// <summary>
		///     Returns all types including the <paramref name="t" /> which are super types of <paramref name="t" />. Works
		///     recursivly.
		/// </summary>
		public static IEnumerable<Type> TypeSuperSet(this Type t)
		{
			if (t == null)
				yield break;

			yield return t;
			foreach (var baseType in t.GetInterfaces().SelectMany(TypeSuperSet).Union(TypeSuperSet(t.BaseType)).Distinct())
				yield return baseType;
		}


		/// <summary>
		///     Use this method if you want to set a property or a field by reflection. This can expecially be useful if the
		///     property or field name is known. Returns true if the property or field was found and the new value was applied.
		/// </summary>
		public static bool Set<TObj, T>(this TObj obj, string propertyOrFieldName, T value)
		{
			obj.MayNotBeNull(nameof(obj));
			propertyOrFieldName.MayNotBeNullOrEmpty(nameof(propertyOrFieldName));
			if (!obj.GetType().ReflectMembers().TryGetValue(propertyOrFieldName, out var member))
				return false;
			return obj.Set(member, value);
		}


		/// <summary>
		///     Use this method if you want to set a property or a field by reflection. This can expecially be useful if the
		///     property or field name is known. Returns true if the property or field was found and the new value was applied.
		/// </summary>
		public static bool Set<TObj, T>(this TObj obj, MemberInfo member, T value)
		{
			obj.MayNotBeNull(nameof(obj));
			member.MayNotBeNull(nameof(member));
			void SetVal(object val)
			{
				switch (member)
				{
					case PropertyInfo property:
						property.GetSetMethod(true).Invoke(obj, new[] {val});
						break;
					case FieldInfo field:
						field.SetValue(obj, val);
						break;
					default: throw new CkNotFoundException($"Unknown member type [{member.GetType()}] detected.");
				}
			}




			if (value == null)
			{
				SetVal(null);
				return true;
			}

			var fieldType = member.GetFieldOrPropertyType();
			var valueType = value.GetType();
			if (valueType.IsDeclareableBy(fieldType) || fieldType.IsNullable(out var realType) && valueType.IsDeclareableBy(realType))
			{
				SetVal(value);
				return true;
			}

			if (valueType == typeof(DBNull))
			{
				SetVal(null);
				return true;
			}

			throw new CkMissingImplementationException($"The field or property ({member.DeclaringType.Name}.{member.Name}) is of type '{fieldType.Name}' but the provided value ({value}) is of type '{valueType.Name}'.");
		}


		/// <summary>
		///     Use this method if you want to get a property or a field value by reflection. This can expecially be useful if the
		///     property or field name is known. Returns true if the property or field was found and the new value was applied.
		/// </summary>
		public static T Get<T>(this object obj, string propertyOrFieldName) =>
			(T) obj.Get(propertyOrFieldName);


		/// <summary>
		///     Use this method if you want to get a property or a field value by reflection. This can expecially be useful if the
		///     property or field name is known. Returns true if the property or field was found and the new value was applied.
		/// </summary>
		public static T Get<T>(this object obj, MemberInfo member)
		{
			obj.MayNotBeNull(nameof(obj));
			member.MayNotBeNull(nameof(member));
			return (T) obj.Get(member);
		}


		/// <summary>
		///     Use this method if you want to get a property or a field value by reflection. This can expecially be useful if the
		///     property or field name is known. Returns true if the property or field was found and the new value was applied.
		/// </summary>
		public static object Get(this object obj, string propertyOrFieldName)
		{
			obj.MayNotBeNull(nameof(obj));
			propertyOrFieldName.MayNotBeNullOrEmpty(nameof(propertyOrFieldName));
			if (!obj.GetType().ReflectMembers().TryGetValue(propertyOrFieldName, out var member))
				throw new CkNotFoundException($"The property or field [{propertyOrFieldName}] is not defined on type [{obj.GetType()}].");

			return obj.Get(member);
		}


		/// <summary>
		///     Use this method if you want to get a property or a field value by reflection. This can expecially be useful if the
		///     property or field name is known. Returns true if the property or field was found and the new value was applied.
		/// </summary>
		public static object Get(this object obj, MemberInfo member)
		{
			obj.MayNotBeNull(nameof(obj));
			member.MayNotBeNull(nameof(member));
			switch (member)
			{
				case PropertyInfo property: return property.GetGetMethod(true).Invoke(obj, null);
				case FieldInfo field: return field.GetValue(obj);
				default: throw new CkNotFoundException($"Unknown member type [{member.GetType()}] detected.");
			}
		}




		private static class Reflection
		{
			public static IEnumerable<MemberInfo> Members(Type type) =>
				Properties(type)?.OfType<MemberInfo>().Union(Fields(type));


			public static IEnumerable<PropertyInfo> Properties(Type type) =>
				type == null ? null : type.GetProperties(Flags);


			public static IEnumerable<FieldInfo> Fields(Type type)
			{
				if (type == null)
					yield break;
				while (type != null)
				{
					foreach (var fieldInfo in type.GetFields(Flags))
						yield return fieldInfo;
					type = type.BaseType;
				}
			}


			public static IEnumerable<Attribute> Attributes(MemberInfo memberInfo)
			{
				switch (memberInfo)
				{
					case Type ti: return ti.GetCustomAttributes(true).OfType<Attribute>();
					case FieldInfo fi: return fi.GetCustomAttributes(true).OfType<Attribute>();
					case PropertyInfo pi: return pi.GetCustomAttributes(true).OfType<Attribute>();
					default: throw new CkMissingImplementationException($"Can not enumerate {nameof(Attribute)}s for memeber of type {memberInfo.GetType()}.");
				}
			}
		}




		private static class Cache
		{
			public static readonly ConcurrentDictionary<MemberInfo, ImmutableArray<Attribute>> MemberAttributes = new ConcurrentDictionary<MemberInfo, ImmutableArray<Attribute>>();
			public static readonly ConcurrentDictionary<Type, ImmutableDictionary<Type, ImmutableDictionary<MemberInfo, ImmutableArray<Attribute>>>> MembersWithAttribute =
				new ConcurrentDictionary<Type, ImmutableDictionary<Type, ImmutableDictionary<MemberInfo, ImmutableArray<Attribute>>>>();
			public static readonly ConcurrentDictionary<Type, ImmutableDictionary<string, MemberInfo>> TypeMembers = new ConcurrentDictionary<Type, ImmutableDictionary<string, MemberInfo>>();
		}
	}
}