﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using CsKit._extensions.typed;
using CsKit._extensions._exceptions;



namespace CsKit._extensions
{
	public static class DbExtensions
	{
		private static readonly Dictionary<Type, DbType> TypeToDbType = new Dictionary<Type, DbType>
		{
			{typeof(byte), DbType.Byte},
			{typeof(sbyte), DbType.Byte},
			{typeof(short), DbType.Int16},
			{typeof(ushort), DbType.Int16},
			{typeof(int), DbType.Int32},
			{typeof(uint), DbType.Int32},
			{typeof(long), DbType.Int64},
			{typeof(ulong), DbType.Int64},
			{typeof(float), DbType.Single},
			{typeof(double), DbType.Double},
			{typeof(decimal), DbType.Decimal},
			{typeof(bool), DbType.Boolean},
			{typeof(string), DbType.String},
			{typeof(char), DbType.StringFixedLength},
			{typeof(Guid), DbType.Guid},
			{typeof(DateTime), DbType.DateTime2},
			{typeof(DateTimeOffset), DbType.DateTimeOffset},
			{typeof(byte[]), DbType.Binary},
			{typeof(byte?), DbType.Byte},
			{typeof(sbyte?), DbType.Byte},
			{typeof(short?), DbType.Int16},
			{typeof(ushort?), DbType.Int16},
			{typeof(int?), DbType.Int32},
			{typeof(uint?), DbType.Int32},
			{typeof(long?), DbType.Int64},
			{typeof(ulong?), DbType.Int64},
			{typeof(float?), DbType.Single},
			{typeof(double?), DbType.Double},
			{typeof(decimal?), DbType.Decimal},
			{typeof(bool?), DbType.Boolean},
			{typeof(char?), DbType.StringFixedLength},
			{typeof(Guid?), DbType.Guid},
			{typeof(DateTime?), DbType.DateTime2},
			{typeof(DateTimeOffset?), DbType.DateTimeOffset},
			{typeof(object), DbType.String}
		};
		private static readonly HashSet<DbType> DbTypeNumerics = new HashSet<DbType>(new[]
		{
			DbType.Byte,
			DbType.Decimal,
			DbType.Double,
			DbType.Int16,
			DbType.Int32,
			DbType.Int64,
			DbType.Single
		});
		


		/// <summary>Converts the <paramref name="type" /> into the matching <see cref="DbType" />.</summary>
		public static string ToSqlServerTypeString(this DbType type, int? maxLength = null, bool isFixedLength = false)
		{
			maxLength?.MayNotBeZero(nameof(maxLength));
			switch (type)
			{
				case DbType.Binary: return $"[{(isFixedLength ? "binary" : "varbinary")}]({(maxLength == null || maxLength > 8000 ? (isFixedLength ? "8000" : "MAX") : maxLength.ToString())})";
				case DbType.Boolean: return $"[bit]";
				case DbType.Byte: return $"[tinyint]";
				case DbType.DateTime2: return $"[datetime2](7)";
				case DbType.Decimal: return $"[decimal]";
				case DbType.Double: return $"[float]";
				case DbType.Guid: return $"[uniqueidentifier]";
				case DbType.Int16: return $"[smallint]";
				case DbType.Int32: return $"[int]";
				case DbType.Int64: return $"[bigint]";
				case DbType.SByte: return $"[tinyint]";
				case DbType.Single: return $"[float]";
				case DbType.String: return $"[{(isFixedLength ? "nchar" : "nvarchar")}]({(maxLength == null || maxLength > 4000 ? (isFixedLength ? "4000" : "MAX") : maxLength.ToString())})";
				case DbType.StringFixedLength: return $"[nchar]({(maxLength == null || maxLength > 4000 ? "4000" : maxLength.ToString())})";
				case DbType.UInt16: return $"[smallint]";
				case DbType.UInt32: return $"[int]";
				case DbType.UInt64: return $"[bigint]";
				default: throw new CsException.NotImplemented($"The {nameof(DbType)}.{type} can not be converted to an sql type string because it is unknown.");
			}
		}





		/// <summary>
		///     Checks if <paramref name="type" /> is one of the following types:
		///     <para>
		///         <c>DbType.Byte, DbType.Decimal, DbType.Double, DbType.Int16, DbType.Int32, DbType.Int64, DbType.Single</c>
		///     </para>
		/// </summary>
		public static bool IsNumericType(this DbType type) =>
			DbTypeNumerics.Contains(type);


		/// <summary>
		///     Checks if <paramref name="type" /> is one of the following types:
		///     <para>
		///         <c>DbType.Date, DbType.DateTime, DbType.DateTime2</c>
		///     </para>
		/// </summary>
		public static bool IsDateType(this DbType type) =>
			type.Equals(DbType.Date) || type.Equals(DbType.DateTime) || type.Equals(DbType.DateTime2);


		/// <summary>Adds an parameter to the <see cref="DbCommand.Parameters" /> list.</summary>
		public static void AddParameter(this DbCommand command, DbType type, string name, object value)
		{
			command.MayNotBeNull(nameof(command));
			name.MayNotBeNullOrEmpty(nameof(name));

			var parameter = command.CreateParameter();
			parameter.DbType = type;
			parameter.Direction = ParameterDirection.Input;
			parameter.ParameterName = name;
			parameter.Value = value ?? DBNull.Value;
			command.Parameters.Add(parameter);
		}
		/// <summary>Converts the <paramref name="type" /> into the matching <see cref="DbType" />.</summary>
		public static DbType ToDbType(this Type type)
		{
			type.MayNotBeNull(nameof(type));
			if (type.IsEnum)
				type = Enum.GetUnderlyingType(type);
			return TypeToDbType[type];
		}
	}
}