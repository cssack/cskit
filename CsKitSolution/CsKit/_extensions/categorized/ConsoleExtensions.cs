﻿using System;



namespace CsKit._extensions.categorized
{
	public static class ConsoleExtensions
	{
		public static int ChooseOption(params string[] options)
		{
			Console.WriteLine("--------- ? ---------");
			for (var i = 0; i < options.Length; i++)
				Console.WriteLine($"  {i+1}) {options[i]}");
			Console.Write("---------------------");

			while (true)
			{
				var keyInfo = Console.ReadKey(true);
				var currentVal = (keyInfo.KeyChar - '0') -1;
				if (currentVal >= 0 && currentVal < options.Length)
				{
					Console.WriteLine($" >> {keyInfo.KeyChar})");
					Console.WriteLine();
					return currentVal;
				}
			}
		}

		public static IDisposable Foreground(ConsoleColor newColor)
		{
			var old = Console.ForegroundColor;
			Console.ForegroundColor = newColor;
			return new DisposeHelper(() => Console.ForegroundColor = old);
		}

		public static IDisposable TaskText(string taskName, ConsoleColor? newColor = null)
		{
			IDisposable colorChange = null;
			if (newColor != null)
				colorChange = Foreground(newColor.Value);

			Console.Write(taskName);
			return new DisposeHelper(() =>
			{
				Console.WriteLine(" > completed");
				colorChange?.Dispose();
			});
		}




		private class DisposeHelper : IDisposable
		{
			private readonly Action _onDispose;


			public DisposeHelper(Action onDispose) =>
				_onDispose = onDispose;


			public void Dispose()
			{
				_onDispose();
			}
		}
	}
}