﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using CsKit._extensions.typed;



namespace CsKit._extensions._exceptions
{
	internal static class CsExceptionExtension{
		public static void MayNotBeNull(this object obj, string argumentName)
		{
			if (!obj.IsNull())
				return;
			throw new CsException.InvalidArgument(argumentName, CsException.InvalidArgument.Reasons.MayNotBeNull);
		}
		public static void MayNotBeNullOrEmpty(this string obj, string argumentName)
		{
			if (!obj.IsNullOrEmpty())
				return;
			throw new CsException.InvalidArgument(argumentName, CsException.InvalidArgument.Reasons.MayNotBeNullOrEmpty);
		}

		public static void MayNotBeZero(this int obj, string argumentName)
		{
			if (!obj.IsZero())
				return;
			throw new CsException.InvalidArgument(argumentName, CsException.InvalidArgument.Reasons.MayNotBeZero);
		}
		public static void MayNotBeNegative(this int obj, string argumentName)
		{
			if (!obj.IsNegative())
				return;
			throw new CsException.InvalidArgument(argumentName, CsException.InvalidArgument.Reasons.MayNotBeNegative);
		}
		public static void MayNotBeZeroOrNegative(this int obj, string argumentName)
		{
			if (!obj.IsZeroOrNegative())
				return;
			throw new CsException.InvalidArgument(argumentName, CsException.InvalidArgument.Reasons.MayNotBeZeroOrNegative);
		}
		public static void MayNotBeGreaterThen(this int obj, string argumentName, int maximum)
		{
			if (obj <= maximum)
				return;
			throw new CsException.InvalidArgument(argumentName, $"May not be greater then [{maximum}].");
		}
		public static void MayNotBeSmallerThen(this int obj, string argumentName, int minimum)
		{
			if (obj >= minimum)
				return;
			throw new CsException.InvalidArgument(argumentName, $"May not be smaller then [{minimum}].");
		}
	}
	public class CsException : Exception
	{
		public CsException()
		{
		}


		public CsException(string message) : base(message)
		{
		}


		public CsException(string message, Exception innerException) : base(message, innerException)
		{
		}


		public CsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}




		public sealed class InvalidArgument : CsException
		{
			public InvalidArgument(string argumentName, string message) : base($"The argument [{argumentName}] is invalid: {message}")
			{
			}


			public InvalidArgument(string argumentName, Reasons reason) : base($"The argument [{argumentName}] is invalid: {reason}")
			{
			}




			public enum Reasons
			{
				[Description("May not be [null].")] MayNotBeNull,
				[Description("May not be [empty].")] MayNotBeEmpty,
				[Description("May not be [null] or [empty].")]
				MayNotBeNullOrEmpty,
				[Description("May not be [0].")] MayNotBeZero,
				[Description("May not be [negative].")] MayNotBeNegative,
				[Description("May not be [0] or [negative].")]
				MayNotBeZeroOrNegative
			}
		}




		public sealed class NotImplemented : CsException
		{
			public NotImplemented(string message) : base(message)
			{
			}
		}
	}
}