﻿using System;



namespace CsKit._engines
{
	/// <summary>
	///     Measures the time by creating and disposing the <see cref="TimeMeasure" />.
	/// </summary>
	public sealed class TimeMeasure : IDisposable
	{
		private readonly Action<TimeSpan> _onCompleted;
		private readonly DateTime _start;


		public TimeMeasure(Action<TimeSpan> onCompleted)
		{
			_onCompleted = onCompleted;
			_start = DateTime.Now;
		}


		public void Dispose() =>
			_onCompleted(DateTime.Now - _start);
	}
}