﻿using System;
using System.Data;
using System.Reflection;
using System.Text;
using CsKit._engines.primitivedb.attributes;
using CsKit._exceptions;
using CsKit._extensions;



namespace CsKit._engines.primitivedb
{
	public class Column
	{
		private DbType? _dbType;
		private bool? _isNullable;
		private string _name;
		private Type _propertyType;


		/// <inheritdoc />
		public Column(MemberInfo memberInfo, ColumnAttribute attribute)
		{
			MemberInfo = memberInfo;
			Attribute = attribute;
		}


		public ColumnAttribute Attribute { get; }
		public MemberInfo MemberInfo { get; }

		/// <summary>
		///     The name of the column. Uses the <see cref="ColumnAttribute.Column" /> name if not null otherwise uses the
		///     name of the column.
		/// </summary>
		public string Name => _name ?? (_name = Attribute?.Column ?? MemberInfo.Name);

		/// <summary>The .net <see cref="Type" /> which will be used to translate between database and the .net framework.</summary>
		public Type PropertyType => _propertyType ?? (_propertyType = MemberInfo.GetFieldOrPropertyType());

		/// <summary>The .net <see cref="Type" /> which will be used to translate between database and the .net framework.</summary>
		public string PropertyName => MemberInfo.Name;

		/// <summary>The <see cref="DbType" /> which will be used to translate between database and the .net framework.</summary>
		public DbType DbType => (_dbType ?? (_dbType = Attribute?.TypeDefined == true ? Attribute.Type : PropertyType.ToDbType())).Value;

		/// <summary>If true null values are allowed.</summary>
		public bool IsNullable => (_isNullable ?? (_isNullable = !IsPrimary && Attribute?.NullableDefined == true && Attribute.IsNullable)).Value;

		/// <summary>
		///     If true this column is the primary key column
		/// </summary>
		public bool IsPrimary => Attribute?.PrimaryDefined == true && Attribute.IsPrimary;

		/// <summary>
		///     If true the <see cref="MaxLength" /> is interpreted as the length of the value. Each value must have exactly that
		///     length.
		/// </summary>
		public bool IsFixedLength => Attribute?.FixedLengthDefined == true && Attribute.IsFixedLength;

		/// <summary>The maximum length of the field. Specifically helpful at strings.</summary>
		public int MaxLength => Attribute?.MaxLengthDefined == true ? Attribute.MaxLength : 0;


		internal string GetCreationString()
		{
			var sb = new StringBuilder($"[{Name}] {DbType.ToSqlServerTypeString(MaxLength == 0 ? null : (int?) MaxLength, IsFixedLength)}");
			if (IsPrimary)
			{
				if (DbType.Equals(DbType.Guid))
					sb.Append(" DEFAULT (newid())");
				else if (DbType.IsNumericType())
					sb.Append(" IDENTITY(1,1)");
				else
					throw new CkMissingImplementationException($"Missing column default for type ({nameof(System.Data.DbType)}.{DbType}) in column creation string.");
			}
			sb.Append(!IsPrimary && IsNullable ? " NULL" : " NOT NULL");
			return sb.ToString();
		}
	}
}