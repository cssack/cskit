﻿using System;
using System.Data;
using System.Linq;
using CsKit._engines.primitivedb.attributes;
using CsKit._exceptions;
using CsKit._extensions;
using CsKit._extensions.typed;



// ReSharper disable StaticMemberInGenericType



namespace CsKit._engines.primitivedb
{
	public static partial class Table<T>
	{
		public static class Query
		{
			public static readonly string OutputColumnName = "CSMODE";
			private static readonly Lazy<string> LazyCreateWithPrimaryKey = new Lazy<string>(() => InitCreate(true));
			private static readonly Lazy<string> LazyCreateWithoutPrimaryKey = new Lazy<string>(() => InitCreate(true));
			private static readonly Lazy<string> LazyUpdate = new Lazy<string>(InitUpdate);
			private static readonly Lazy<string> LazyCreateOrUpdate = new Lazy<string>(InitCreateOrUpdate);
			private static readonly Lazy<string> LazyDelete = new Lazy<string>(InitDelete);
			private static readonly Lazy<string> LazyCreateTableIfNotExist = new Lazy<string>(InitCreateTableIfNotExist);
			private static readonly Lazy<string> LazyCountRows = new Lazy<string>(InitCountRows);
			private static readonly Lazy<string> LazySelectRow = new Lazy<string>(InitSelectRow);



			public static string CreateOrUpdate => LazyCreateOrUpdate.Value;
			public static string CreateTableIfNotExist => LazyCreateTableIfNotExist.Value;
			public static string CreateWithoutPrimaryKey => LazyCreateWithoutPrimaryKey.Value;
			public static string CreateWithPrimaryKey => LazyCreateWithPrimaryKey.Value;
			public static string Delete => LazyDelete.Value;
			public static string Update => LazyUpdate.Value;
			public static string CountRows => LazyCountRows.Value;
			public static string SelectRow => LazySelectRow.Value;


			private static string InitCreate(bool includePkKey)
			{
				var pre = Columns.Where(x => !x.Attribute.Behaviour.HasFlag(CrudBehaviours.ExcludeFromCreate)).ToList();
				if (includePkKey)
					pre.Prepend(PkColumn);

				return $"INSERT INTO [{Name}] (" +
				       $"	{pre.Select(x => $"[{x.Name}]").Join()}" +
				       $") OUTPUT 1 AS [{OutputColumnName}] {(LoadOnUpdateColumns.Length > 0 ? $", {LoadOnUpdateColumns.Select(x => $"INSERTED.[{x.Name}] as [{x.Name}]").Join()}":"")}" +
				       $"VALUES (" +
				       $"	{pre.Select(x => $"@{x.Name}").Join()}" +
				       $")";
			}


			private static string InitUpdate()
			{
				return $"UPDATE [{Name}] SET " +
				       Columns.Where(x => !x.Attribute.Behaviour.HasFlag(CrudBehaviours.ExcludeFromUpdate))
				              .Select(x => $"[{x.Name}] = @{x.PropertyName}")
				              .Union(Columns.Where(x => x.Attribute.Behaviour.HasFlag(CrudBehaviours.IncrementOnUpdate))
				                            .Select(x =>
				                            {
												if (x.DbType.IsNumericType())
													return $"[{x.Name}] = [{x.Name}] + 1";
												if (x.DbType.IsDateType())
													return $"[{x.Name}] = GETDATE()";
												throw new CkMissingImplementationException($"Behaviour ({nameof(CrudBehaviours)}.{CrudBehaviours.IncrementOnUpdate}) for {nameof(DbType)}.{x.DbType} is undefined.");
				                            }))
				              .Join() + " " +
				       $"OUTPUT 2 AS [{OutputColumnName}] {(LoadOnUpdateColumns.Length > 0 ? $", {LoadOnUpdateColumns.Select(x => $"INSERTED.[{x.Name}] as [{x.Name}]").Join()}":"")}" +
				       $"WHERE [{PkColumn.Name}] = @{PkColumn.PropertyName}";
			}

			private static string InitCreateOrUpdate() =>
				$"IF EXISTS(SELECT * FROM [{Name}] WHERE [{PkColumn.Name}] = @{PkColumn.PropertyName}) " +
				$"	{Update} " +
				$"ELSE " +
				$"	{CreateWithPrimaryKey};";

			private static string InitDelete() =>
				$"DELETE FROM [{Name}] WHERE [{PkColumn.Name}] = @{PkColumn.PropertyName}";

			private static string InitCreateTableIfNotExist() =>
				$"IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE [TABLE_NAME] = '{Name}') BEGIN " +
				$"CREATE TABLE [{Name}] " +
				$"(" + PkColumn.GetCreationString() +
				$"	, " + Columns.Select(x => x.GetCreationString()).Join() +
				$"	, CONSTRAINT [PK_{Name}] PRIMARY KEY CLUSTERED ([{PkColumn.Name}] ASC)" +
				$") END";

			private static string InitCountRows() =>
				$"SELECT COUNT(*) FROM [{Name}]";

			private static string InitSelectRow() =>
				$"SELECT * FROM [{Name}] WHERE [{PkColumn.Name}] = @{PkColumn.PropertyName}";
		}
	}
}