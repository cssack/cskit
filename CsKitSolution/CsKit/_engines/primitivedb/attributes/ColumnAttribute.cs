﻿using System;
using System.Data;



namespace CsKit._engines.primitivedb.attributes
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class ColumnAttribute : Attribute
	{
		private CrudBehaviours? _behaviour;
		private bool? _isFixedLength;
		private bool? _isNullable;
		private bool? _isPrimary;
		private int? _maxLength;
		private DbType? _type;

		/// <summary>
		///     The database table column name of the property.
		/// </summary>
		public string Column { get; set; }

		/// <summary>
		///     The database type of the property.
		/// </summary>
		public DbType Type
		{
			get => _type.GetValueOrDefault();
			set => _type = value;
		}

		/// <summary>
		///     If true the column can be null.
		/// </summary>
		public bool IsNullable
		{
			get => _isNullable.GetValueOrDefault();
			set => _isNullable = value;
		}

		/// <summary>
		///     If true this is the primary column (only one allowed per class).
		/// </summary>
		public bool IsPrimary
		{
			get => _isPrimary.GetValueOrDefault();
			set => _isPrimary = value;
		}

		/// <summary>
		///     If true the <see cref="MaxLength" /> is interpreted as the length of the value. Each value must have exactly that
		///     length.
		/// </summary>
		public bool IsFixedLength
		{
			get => _isFixedLength.GetValueOrDefault();
			set => _isFixedLength = value;
		}

		/// <summary>
		///     The maximum length of the field. Specifically helpful at strings.
		/// </summary>
		public int MaxLength
		{
			get => _maxLength.GetValueOrDefault();
			set => _maxLength = value;
		}

		/// <summary>
		///     Allows to configure a column with special functionality which will later be applied to the crud operations.
		/// </summary>
		public CrudBehaviours Behaviour
		{
			get => _behaviour.GetValueOrDefault(CrudBehaviours.None);
			set => _behaviour = value;
		}


		public bool NullableDefined => _isNullable != null;
		public bool TypeDefined => _type != null;
		public bool PrimaryDefined => _isPrimary != null;
		public bool FixedLengthDefined => _isFixedLength != null;
		public bool MaxLengthDefined => _maxLength != null;
		public bool BehaviourDefined => _behaviour != null;
	}




	/// <summary>Different behaviours for special functionality with Crud operations</summary>
	[Flags]
	public enum CrudBehaviours
	{
		/// <summary>No special behavior.</summary>
		None = 1 << 0,
		/// <summary>The column will not be changed on row creation.</summary>
		ExcludeFromCreate = 1 << 1,
		/// <summary>The column will not be changed on row update.</summary>
		ExcludeFromUpdate = 1 << 2,
		/// <summary>
		///     Instead of passing the value to an update statement(!), the column will be reloaded from database whenever the
		///     row is created or updated.
		/// </summary>
		LoadOnUpdate = (1 << 3) | ExcludeFromUpdate,
		/// <summary>
		///     Instead of passing the value to an update statement(!), the current database value will be incremented by one if
		///     type is numeric. If type is a datetype will be set to the current date.
		/// </summary>
		IncrementOnUpdate = (1 << 4) | ExcludeFromUpdate | LoadOnUpdate
	}
}