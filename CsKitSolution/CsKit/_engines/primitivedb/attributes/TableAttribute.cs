﻿using System;



namespace CsKit._engines.primitivedb.attributes
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class TableAttribute : Attribute
	{
		/// <summary>
		///     The database table name for the class.
		/// </summary>
		public string Name { get; set; }
	}
}