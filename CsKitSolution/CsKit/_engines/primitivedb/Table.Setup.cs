﻿using System.Data.Common;



namespace CsKit._engines.primitivedb
{
	public static partial class Table<T>
	{
		public static class Setup
		{
			/// <summary>Creates a table for this entity in a database.</summary>
			public static void CreateTableIfNotExists(DbConnection connection)
			{
				using (var cmd = Raw.Command(connection))
					CreateTableIfNotExists(cmd);
			}


			/// <summary>Creates a table for this entity in a database.</summary>
			public static void CreateTableIfNotExists(DbTransaction transaction)
			{
				using (var cmd = Raw.Command(transaction))
					CreateTableIfNotExists(cmd);
			}


			private static void CreateTableIfNotExists(DbCommand command)
			{
				command.CommandText = Query.CreateTableIfNotExist;
				command.ExecuteNonQuery();
			}
		}
	}
}